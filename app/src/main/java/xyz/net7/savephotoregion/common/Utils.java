package xyz.net7.savephotoregion.common;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class Utils {

    public static float ratioScreen() {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        return displayMetrics.widthPixels/displayMetrics.heightPixels;
    }

    public static int widthScreen() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int heightScreen() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static float convertDpToPixel(float dp, Context context) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static float convertPixelsToDp(float px, Context context) {
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }
}
