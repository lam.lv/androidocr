package xyz.net7.savephotoregion.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import xyz.net7.savephotoregion.R;
import xyz.net7.savephotoregion.controller.BitmapController;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imgPicture = findViewById(R.id.img_picture);
        BitmapController bitmapController = BitmapController.newInstance();
        imgPicture.setImageBitmap(bitmapController.getBitmap());
    }
}
