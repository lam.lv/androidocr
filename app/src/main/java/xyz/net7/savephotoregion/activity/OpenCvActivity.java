package xyz.net7.savephotoregion.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import xyz.net7.savephotoregion.R;
import xyz.net7.savephotoregion.common.Utils;
import xyz.net7.savephotoregion.controller.BitmapController;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class OpenCvActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private TypeCamera typeCamera;

    private String TAG = "OpenCvActivity";

    private CameraBridgeViewBase openCvCameraView;
    private View borderCamera;
    private TextView tvCountDown;

    private ConstraintLayout constraintLayout;
    private LinearLayout contain_border_camera_linear_layout;

    private CascadeClassifier faceCascadeClassifier;
    private Mat aInputFrame, mROI;

    private BitmapController bitmapController;
    private CountDownTimer countDownTimer;
    private Bitmap picture;

    private static final int REQUEST_CODE = 100;
    private String[] requestPermissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    //initializeOpenCVDependencies();
                    openCvCameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_cv);

        initView();
        initData();
        checkPermission();
        setFrameForMultiScreen();
    }

    private void initView() {
        borderCamera = findViewById(R.id.border_camera);
        contain_border_camera_linear_layout = findViewById(R.id.contain_border_camera_linear_layout);
        constraintLayout = findViewById(R.id.constraintLayout);
        tvCountDown = findViewById(R.id.tv_count_down);
    }

    private void initData() {
        bitmapController = BitmapController.newInstance();
    }

    private void setFrameForMultiScreen() {
        typeCamera = TypeCamera.PASSPORT;
        float ratioCameraFrame = 32.f / 20.f;
        if (typeCamera == TypeCamera.PASSPORT) {
            float width = getResources().getDimension(R.dimen.bg_passport_width);
            float height = getResources().getDimension(R.dimen.bg_passport_height);
            ratioCameraFrame = width/height;
            borderCamera.setBackground(getResources().getDrawable(R.drawable.bg_passport));
        }
        int heightScreen = Utils.heightScreen();
        int widthScreen = Utils.widthScreen();
        Log.d(TAG, "Screen: " + widthScreen + " - " + heightScreen);

        int marginTop = (int) Utils.convertDpToPixel(50, this);
        Log.d(TAG, "marginTop: " + marginTop);

        int heightCameraFrame = heightScreen - marginTop * 2;
        float widthCameraFrame = heightCameraFrame * ratioCameraFrame;
        Log.d(TAG, "CameraFrame: " + widthCameraFrame + " - " + heightCameraFrame);

        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams((int) widthCameraFrame, heightCameraFrame);
        contain_border_camera_linear_layout.setLayoutParams(params);


        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(constraintLayout);
        constraintSet.connect(R.id.contain_border_camera_linear_layout, ConstraintSet.TOP, R.id.constraintLayout, ConstraintSet.TOP, 0);
        constraintSet.connect(R.id.contain_border_camera_linear_layout, ConstraintSet.START, R.id.constraintLayout, ConstraintSet.START, 0);
        constraintSet.connect(R.id.contain_border_camera_linear_layout, ConstraintSet.END, R.id.constraintLayout, ConstraintSet.END, 0);
        constraintSet.connect(R.id.contain_border_camera_linear_layout, ConstraintSet.BOTTOM, R.id.constraintLayout, ConstraintSet.BOTTOM, 0);
        constraintSet.applyTo(constraintLayout);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (openCvCameraView != null) {
            openCvCameraView.disableView();
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        //TODO
        aInputFrame = new Mat(width, height, CvType.CV_8UC4);

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        aInputFrame = inputFrame.rgba();
        setROI();
        return aInputFrame;
    }

    private void setROI() {
        // calculate aspect ratio
        float koefX = (float) aInputFrame.width() / (float) openCvCameraView.getWidth();
        float koefY = (float) aInputFrame.height() / (float) openCvCameraView.getHeight();

        // get viewfinder border size and position on the screen
        int x1 = contain_border_camera_linear_layout.getLeft();
        int y1 = contain_border_camera_linear_layout.getTop();

        int x2 = borderCamera.getWidth();
        int y2 = borderCamera.getHeight();

        //calculate position and size for cropping
        int cropStartX = Math.round(x1 * koefX);
        int cropStartY = Math.round(y1 * koefX);

        int cropWidthX = Math.round(x2 * koefX);
        int cropHeightY = Math.round(y2 * koefX);

        Rect rect1 = new Rect(cropStartX, cropStartY, cropWidthX, cropHeightY);
        //Imgproc.rectangle(aInputFrame, rect1.tl(), rect1.br(), new Scalar(100, 255, 200, 255));
        mROI = aInputFrame.submat(rect1);
    }

    //region Button take picture
    public void btnTakePicture(final View view) {
        view.setEnabled(false);
        countDownTimer = new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvCountDown.setText(String.valueOf(millisUntilFinished / 1000));

                Bitmap bitmap = Bitmap.createBitmap(mROI.cols(), mROI.rows(), Bitmap.Config.ARGB_8888);
                org.opencv.android.Utils.matToBitmap(mROI, bitmap);
                Log.d("BrightnessImage", calculateBrightness(bitmap) + "");
                checkBlur(bitmap);
                picture = bitmap;
            }

            @Override
            public void onFinish() {
                tvCountDown.setText("");
                view.setEnabled(true);
                bitmapController.setBitmap(picture);
                Intent anotherIntent = new Intent(OpenCvActivity.this, MainActivity.class);
                startActivity(anotherIntent);
            }
        }.start();
    }
    //endregion

    //region Checking methods
    public int calculateBrightnessEstimate(android.graphics.Bitmap bitmap, int pixelSpacing) {
        int R = 0;
        int G = 0;
        int B = 0;
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        int n = 0;
        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        for (int i = 0; i < pixels.length; i += pixelSpacing) {
            int color = pixels[i];
            R += Color.red(color);
            G += Color.green(color);
            B += Color.blue(color);
            n++;
        }
        return (R + B + G) / (n * 3);
    }

    public int calculateBrightness(Bitmap bitmap) {
        return calculateBrightnessEstimate(bitmap, 1);
    }

    private void checkBlur(Bitmap bitmap) {
        Bitmap myBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        int l = CvType.CV_8UC1; //8-bit grey scale image
        Mat matImage = new Mat();
        org.opencv.android.Utils.bitmapToMat(myBitmap, matImage);
        Mat matImageGrey = new Mat();
        Imgproc.cvtColor(matImage, matImageGrey, Imgproc.COLOR_BGR2GRAY);

        Bitmap destImage;
        destImage = Bitmap.createBitmap(myBitmap);
        Mat dst2 = new Mat();
        org.opencv.android.Utils.bitmapToMat(destImage, dst2);
        Mat laplacianImage = new Mat();
        dst2.convertTo(laplacianImage, l);
        Imgproc.Laplacian(matImageGrey, laplacianImage, CvType.CV_8U);
        Mat laplacianImage8bit = new Mat();
        laplacianImage.convertTo(laplacianImage8bit, l);

        Bitmap bmp = Bitmap.createBitmap(laplacianImage8bit.cols(), laplacianImage8bit.rows(), Bitmap.Config.ARGB_8888);
        org.opencv.android.Utils.matToBitmap(laplacianImage8bit, bmp);
        int[] pixels = new int[bmp.getHeight() * bmp.getWidth()];
        bmp.getPixels(pixels, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        int maxLap = -16777216; // 16m
        for (int pixel : pixels) {
            if (pixel > maxLap)
                maxLap = pixel;
        }

        int soglia = -6118750;
        if (maxLap <= soglia) {
            System.out.println("is blur image");
            Toast.makeText(OpenCvActivity.this, "Blur Image", Toast.LENGTH_SHORT).show();
            Log.d("CheckImage", "Blur Image");
        } else {
            Toast.makeText(OpenCvActivity.this, "Clear Image", Toast.LENGTH_SHORT).show();
            Log.d("CheckImage", "Clear Image");
        }
    }
    //endregion

    private void initializeOpenCVDependencies() {
        try {
            // Copy the resource into a temp file so OpenCV can load it
            InputStream is = getResources().openRawResource(R.raw.haarcascade_frontalface_default);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, "haarcascade_frontalface_default.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);


            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            // Load the cascade classifier
            faceCascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
        } catch (Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }

        // And we are ready to go
        openCvCameraView.enableView();
    }

    //region Check and request Permission
    private void checkPermission() {
        List<String> requestPermissionList = new ArrayList<>();

        // CHECK PERMISSION
        for (String permission : requestPermissions) {

            int permissionCode = ContextCompat.checkSelfPermission(this, permission);

            if (permissionCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissionList.add(permission);
            }
        }

        // REQUEST PERMISSION
        if (!requestPermissionList.isEmpty()) {
            String[] permissions = requestPermissionList.toArray(new String[requestPermissionList.size()]);

            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE);
        } else {
            final int[] grantResults = new int[requestPermissions.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE, requestPermissions,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                openCvCameraView = findViewById(R.id.java_camera);
                openCvCameraView.setVisibility(SurfaceView.VISIBLE);
                openCvCameraView.setCvCameraViewListener(this);
                break;
        }
    }
    //endregion

    //region detection methods (Not Use)
    private Mat findRectangle(Mat src) {

        Mat blurred = src.clone();
        Imgproc.medianBlur(src, blurred, 9);

        Mat gray0 = new Mat(blurred.size(), CvType.CV_8U), gray = new Mat();

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        List<Mat> blurredChannel = new ArrayList<Mat>();
        blurredChannel.add(blurred);
        List<Mat> gray0Channel = new ArrayList<Mat>();
        gray0Channel.add(gray0);

        MatOfPoint2f approxCurve;

        double maxArea = 0;
        int maxId = -1;

        for (int c = 0; c < 3; c++) {
            int ch[] = {c, 0};
            Core.mixChannels(blurredChannel, gray0Channel, new MatOfInt(ch));

            int thresholdLevel = 1;
            for (int t = 0; t < thresholdLevel; t++) {
                if (t == 0) {
                    Imgproc.Canny(gray0, gray, 10, 20, 3, true); // true ?
                    Imgproc.dilate(gray, gray, new Mat(), new Point(-1, -1), 1); // 1
                    // ?
                } else {
                    Imgproc.adaptiveThreshold(gray0, gray, thresholdLevel,
                            Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                            Imgproc.THRESH_BINARY,
                            (src.width() + src.height()) / 200, t);
                }

                Imgproc.findContours(gray, contours, new Mat(),
                        Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

                for (MatOfPoint contour : contours) {
                    MatOfPoint2f temp = new MatOfPoint2f(contour.toArray());

                    double area = Imgproc.contourArea(contour);
                    approxCurve = new MatOfPoint2f();
                    Imgproc.approxPolyDP(temp, approxCurve,
                            Imgproc.arcLength(temp, true) * 0.02, true);

                    if (approxCurve.total() == 4 && area >= maxArea) {
                        double maxCosine = 0;

                        List<Point> curves = approxCurve.toList();
                        for (int j = 2; j < 5; j++) {

                            double cosine = Math.abs(angle(curves.get(j % 4),
                                    curves.get(j - 2), curves.get(j - 1)));
                            maxCosine = Math.max(maxCosine, cosine);
                        }

                        if (maxCosine < 0.3) {
                            maxArea = area;
                            maxId = contours.indexOf(contour);
                        }
                    }
                }
            }
        }

        if (maxId >= 0) {
            Rect rect = Imgproc.boundingRect(contours.get(maxId));

            Imgproc.rectangle(src, rect.tl(), rect.br(), new Scalar(255, 0, 0,
                    .8), 4);


            int mDetectedWidth = rect.width;
            int mDetectedHeight = rect.height;

            Log.d("TESTTTT", "Rectangle width :" + mDetectedWidth + " Rectangle height :" + mDetectedHeight);

        }


        return src;


    }

    private double angle(Point p1, Point p2, Point p0) {
        double dx1 = p1.x - p0.x;
        double dy1 = p1.y - p0.y;
        double dx2 = p2.x - p0.x;
        double dy2 = p2.y - p0.y;
        return (dx1 * dx2 + dy1 * dy2)
                / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2)
                + 1e-10);
    }

    private void faceDetect(Mat aInputFrame) {
        MatOfRect faces = new MatOfRect();
        // Use the classifier to detect faces
        if (faceCascadeClassifier != null) {
            faceCascadeClassifier.detectMultiScale(mROI, faces, 1.1, 3, 0, new Size(60, 60));
        }

        // If there are any faces found, draw a rectangle around it
        Rect[] facesArray = faces.toArray();
        Log.d("FaceDetect", facesArray.length + "");
        for (int i = 0; i < facesArray.length; i++) {
            Rect rect = facesArray[i];
            Imgproc.rectangle(aInputFrame, rect.tl(), rect.br(), new Scalar(0, 255, 0, 255), 3);
        }
    }
    //endregion

    public enum TypeCamera {
        LICENSE_FRONT,
        PASSPORT
    }
}
