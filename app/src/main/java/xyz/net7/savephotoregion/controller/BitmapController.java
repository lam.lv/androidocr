package xyz.net7.savephotoregion.controller;

import android.graphics.Bitmap;

public class BitmapController {

    private static BitmapController bitmapController;

    private Bitmap bitmap;

    public static BitmapController newInstance() {
        if (bitmapController == null) {
            bitmapController = new BitmapController();
        }
        return bitmapController;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public void destroyBitmapController() {
        if (bitmapController != null) {
            bitmapController = null;
        }
    }
}
